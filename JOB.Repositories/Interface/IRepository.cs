﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace JOB.Repositories.Interface
{
    public interface IRepository
    {
        IQueryable<T> All<T>() where T : class;

        T Single<T>(Expression<Func<T, bool>> expression) where T : class;

        T Find<T>(Expression<Func<T, bool>> predicate) where T : class;

        void Update<T>(T TObject) where T : class;

        void Create<T>(T TObject) where T : class;

        void Delete<T>(T TObject) where T : class;

        public IQueryable<T> Set<T>() where T : class;
    }
}
