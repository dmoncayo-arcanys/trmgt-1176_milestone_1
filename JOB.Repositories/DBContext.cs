﻿using Microsoft.EntityFrameworkCore;
using JOB.Entities.Entity;
using JOB.Common.Constants;

namespace JOB.Repositories
{
    public class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        public DbSet<Applicant> Applicant { get; set; }

        public DbSet<Employer> Employer { get; set; }

        public DbSet<Post> Post { get; set; }

        public DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Applicant>().ToTable(Constants.TABLE_APPLICATION);
            modelBuilder.Entity<Employer>().ToTable(Constants.TABLE_EMPLOYER);
            modelBuilder.Entity<Post>().ToTable(Constants.TABLE_POST);
            modelBuilder.Entity<User>().ToTable(Constants.TABLE_USER);
        }
    }
}
