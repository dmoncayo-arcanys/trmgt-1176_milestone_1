﻿using JOB.Entities.Entity;
using System;
using System.Linq;

namespace JOB.Repositories
{
    public class DBInitializer
    {
        public static void Initialize(DBContext context)
        {
            // Look for any employers.
            if (context.Post.Any())
            {
                return;   // DB has been seeded
            }

            var employerList = new Employer[]
            {
                new Employer {
                    EmailAddress = "hiring@google.com",
                    MobileNumber = "09182993044",
                    FirstName = "Mike",
                    LastName = "Spencer",
                    CompanyName = "Google"
                },
                new Employer {
                    EmailAddress = "hr@facebook.com",
                    MobileNumber = "09175523332",
                    FirstName = "Laura",
                    LastName = "Smith",
                    CompanyName = "Facebook"
                },
                new Employer {
                    EmailAddress = "hello@tesla.com",
                    MobileNumber = "09184533488",
                    FirstName = "Elon",
                    LastName = "Musk",
                    CompanyName = "Tesla"
                },
            };

            foreach (Employer employer in employerList)
            {
                context.Employer.Add(employer);
            }
            context.SaveChanges();

            var postList = new Post[]
            {
                new Post {
                    EmployerID = employerList.Single(s => s.EmailAddress == "hello@tesla.com").ID,
                    Timestamp = DateTime.Now,
                    Title = ".NET Developer",
                    Description = "You can build the application by following the steps in a series of tutorials.",
                    Salary = 130000
                },
                new Post {
                    EmployerID = employerList.Single(s => s.EmailAddress == "hiring@google.com").ID,
                    Timestamp = DateTime.Now,
                    Title = "Angular Developer",
                    Description = "You can build the application by following the steps in a series of tutorials.",
                    Salary = 120000
                },
                new Post {
                    EmployerID = employerList.Single(s => s.EmailAddress == "hiring@google.com").ID,
                    Timestamp = DateTime.Now,
                    Title = "Senior Java Developer",
                    Description = "You can build the application by following the steps in a series of tutorials.",
                    Salary = 190000
                },
                new Post {
                    EmployerID = employerList.Single(s => s.EmailAddress == "hr@facebook.com").ID,
                    Timestamp = DateTime.Now,
                    Title = "Web Developer",
                    Description = "You can build the application by following the steps in a series of tutorials.",
                    Salary = 130000
                },
                new Post {
                    EmployerID = employerList.Single(s => s.EmailAddress == "hr@facebook.com").ID,
                    Timestamp = DateTime.Now,
                    Title = "Full Stack Developer",
                    Description = "You can build the application by following the steps in a series of tutorials.",
                    Salary = 120000
                },
                new Post {
                    EmployerID = employerList.Single(s => s.EmailAddress == "hr@facebook.com").ID,
                    Timestamp = DateTime.Now,
                    Title = "Junior PHP Developer",
                    Description = "You can build the application by following the steps in a series of tutorials.",
                    Salary = 190000
                },
            };

            foreach (Post post in postList)
            {
                context.Post.Add(post);
            }
            context.SaveChanges();

            var applicantList = new Applicant[]
            {
                new Applicant {
                    PostID = postList.Single(s => s.Title == "Junior PHP Developer").PostID,
                    EmailAddress = "juan.ponce@gmail.com",
                    MobileNumber = "09752243311",
                    FirstName = "Juan",
                    LastName = "Ponce",
                    Message = "You can build the application by following the steps in a series of tutorials.",
                    YearsOfExperience = 2,
                    ExpectedSalary = 190000,
                    StartDate = DateTime.Parse("2021-06-20"),
                },
            };

            foreach (Applicant applicant in applicantList)
            {
                context.Applicant.Add(applicant);
            }
            context.SaveChanges();
        }
    }
}
