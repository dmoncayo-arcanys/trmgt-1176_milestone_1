﻿using JOB.Repositories.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace JOB.Repositories.Repository
{
    public class Repository : IRepository
    {
        private readonly DBContext _context;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context"></param>
        public Repository(DBContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get single value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression"></param>
        /// <returns>T</returns>
        public T Single<T>(Expression<Func<T, bool>> expression) where T : class
        {
            return All<T>().FirstOrDefault(expression);
        }

        /// <summary>
        /// Get all value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>IQueryable<T></returns>
        public IQueryable<T> All<T>() where T : class
        {
            return _context.Set<T>().AsQueryable();
        }

        /// <summary>
        /// Find single value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="predicate"></param>
        /// <returns>T</returns>
        public virtual T Find<T>(Expression<Func<T, bool>> predicate) where T : class
        {
            return _context.Set<T>().FirstOrDefault<T>(predicate);
        }

        /// <summary>
        /// Update record.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TObject"></param>
        public virtual void Update<T>(T TObject) where T : class
        {
            var entry = _context.Entry(TObject);
            _context.Set<T>().Attach(TObject);
            entry.State = EntityState.Modified;
            _context.SaveChanges();
        }

        /// <summary>
        /// Save record.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TObject"></param>
        public virtual void Create<T>(T TObject) where T : class
        {
            _context.Set<T>().Add(TObject);
            _context.SaveChanges();
        }

        /// <summary>
        /// Delete record.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="TObject"></param>
        public virtual void Delete<T>(T TObject) where T : class
        {
            _context.Set<T>().Remove(TObject);
            _context.SaveChanges();
        }

        /// <summary>
        /// Get Set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>IQueryable</returns>
        public IQueryable<T> Set<T>() where T : class
        {
            return _context.Set<T>();
        }
    }
}
