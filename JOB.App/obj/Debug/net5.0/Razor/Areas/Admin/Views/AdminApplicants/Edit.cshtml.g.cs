#pragma checksum "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "430eda65cbd744e0e5ac0451f039850c3a837ad2"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_AdminApplicants_Edit), @"mvc.1.0.view", @"/Areas/Admin/Views/AdminApplicants/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"430eda65cbd744e0e5ac0451f039850c3a837ad2", @"/Areas/Admin/Views/AdminApplicants/Edit.cshtml")]
    public class Areas_Admin_Views_AdminApplicants_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<JOB.Entities.Entity.Applicant>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Edit.cshtml"
  
    ViewData["Title"] = "Edit";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Edit</h1>

<h4>Applicant</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Edit"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>
            <div class=""form-group"">
                <label asp-for=""PostID"" class=""control-label""></label>
                <select asp-for=""PostID"" class=""form-control"" asp-items=""ViewBag.PostID""></select>
                <span asp-validation-for=""PostID"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""Message"" class=""control-label""></label>
                <input asp-for=""Message"" class=""form-control"" />
                <span asp-validation-for=""Message"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""YearsOfExperience"" class=""control-label""></label>
                <input asp-for=""YearsOfExperience"" class=""form-control"" />
                <span asp-validat");
            WriteLiteral(@"ion-for=""YearsOfExperience"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""ExpectedSalary"" class=""control-label""></label>
                <input asp-for=""ExpectedSalary"" class=""form-control"" />
                <span asp-validation-for=""ExpectedSalary"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""StartDate"" class=""control-label""></label>
                <input asp-for=""StartDate"" class=""form-control"" />
                <span asp-validation-for=""StartDate"" class=""text-danger""></span>
            </div>
            <input type=""hidden"" asp-for=""ID"" />
            <div class=""form-group"">
                <label asp-for=""EmailAddress"" class=""control-label""></label>
                <input asp-for=""EmailAddress"" class=""form-control"" />
                <span asp-validation-for=""EmailAddress"" class=""text-danger""></span>
            </div>
            <div class=""");
            WriteLiteral(@"form-group"">
                <label asp-for=""MobileNumber"" class=""control-label""></label>
                <input asp-for=""MobileNumber"" class=""form-control"" />
                <span asp-validation-for=""MobileNumber"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""FirstName"" class=""control-label""></label>
                <input asp-for=""FirstName"" class=""form-control"" />
                <span asp-validation-for=""FirstName"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <label asp-for=""LastName"" class=""control-label""></label>
                <input asp-for=""LastName"" class=""form-control"" />
                <span asp-validation-for=""LastName"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <input type=""submit"" value=""Save"" class=""btn btn-primary"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-");
            WriteLiteral("action=\"Index\">Back to List</a>\r\n</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 73 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Edit.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<JOB.Entities.Entity.Applicant> Html { get; private set; }
    }
}
#pragma warning restore 1591
