#pragma checksum "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "a30464e5f1840f328fc4f15279cd4b29b916d999"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_Admin_Views_AdminApplicants_Delete), @"mvc.1.0.view", @"/Areas/Admin/Views/AdminApplicants/Delete.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"a30464e5f1840f328fc4f15279cd4b29b916d999", @"/Areas/Admin/Views/AdminApplicants/Delete.cshtml")]
    public class Areas_Admin_Views_AdminApplicants_Delete : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<JOB.Entities.Entity.Applicant>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
  
    ViewData["Title"] = "Delete";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Delete</h1>\r\n\r\n<h3>Are you sure you want to delete this?</h3>\r\n<div>\r\n    <h4>Applicant</h4>\r\n    <hr />\r\n    <dl class=\"row\">\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 15 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Message));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 18 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Message));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 21 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.YearsOfExperience));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 24 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.YearsOfExperience));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 27 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.ExpectedSalary));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 30 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.ExpectedSalary));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 33 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.StartDate));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 36 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.StartDate));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 39 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.Post));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 42 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.Post.PostID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd class>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 45 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.EmailAddress));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 48 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.EmailAddress));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 51 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.MobileNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 54 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.MobileNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 57 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.FirstName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 60 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.FirstName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 63 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayNameFor(model => model.LastName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 66 "C:\Users\DMoncayo\Documents\Projects\TRMGT-1176_MILESTONE_1\JOB.API\JOB.App\Areas\Admin\Views\AdminApplicants\Delete.cshtml"
       Write(Html.DisplayFor(model => model.LastName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n    \r\n    <form asp-action=\"Delete\">\r\n        <input type=\"hidden\" asp-for=\"ID\" />\r\n        <input type=\"submit\" value=\"Delete\" class=\"btn btn-danger\" /> |\r\n        <a asp-action=\"Index\">Back to List</a>\r\n    </form>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<JOB.Entities.Entity.Applicant> Html { get; private set; }
    }
}
#pragma warning restore 1591
