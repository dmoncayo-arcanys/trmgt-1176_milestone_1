﻿using JOB.Common.ActionFilters;
using JOB.Entities.Entity;
using JOB.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace JOB.App.Controllers
{
    public class ApplicantsController : Controller
    {
        private readonly IApplicantService _applicantService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="applicantService"></param>
        public ApplicantsController(IApplicantService applicantService)
        {
            _applicantService = applicantService;
        }

        // GET: Applicants
        public IActionResult Index()
        {
            var errorMessage = string.Empty;
            return View(_applicantService.GetAll(ref errorMessage));
        }

        // GET: Applicants/Details/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var applicant = _applicantService.Get((int)id, ref errorMessage);
            if (applicant == null)
            {
                return NotFound();
            }

            SetErrorMessage(errorMessage);
            return View(applicant);
        }

        // GET: Applicants/Create
        public IActionResult Create()
        {
            SetPostDropdown(new Applicant());
            return View();
        }

        // POST: Applicants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("PostID,Message,YearsOfExperience,ExpectedSalary,StartDate,ID,EmailAddress,MobileNumber,FirstName,LastName")] Applicant applicant)
        {
            if (ModelState.IsValid)
            {
                var errorMessage = string.Empty;
                _applicantService.Create(applicant, ref errorMessage);
                SetErrorMessage(errorMessage);
                return RedirectToAction(nameof(Index));
            }
            SetPostDropdown(applicant);
            return View(applicant);
        }

        // GET: Applicants/Edit/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var applicant = _applicantService.Get((int)id, ref errorMessage);
            SetErrorMessage(errorMessage);
            if (applicant == null)
            {
                return NotFound();
            }
            SetPostDropdown(applicant);
            return View(applicant);
        }

        // POST: Applicants/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("PostID,Message,YearsOfExperience,ExpectedSalary,StartDate,ID,EmailAddress,MobileNumber,FirstName,LastName")] Applicant applicant)
        {
            if (id != applicant.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var errorMessage = string.Empty;
                _applicantService.Update(id, applicant, ref errorMessage);
                SetErrorMessage(errorMessage);
                return RedirectToAction(nameof(Index));
            }
            SetPostDropdown(applicant);
            return View(applicant);
        }

        // GET: Applicants/Delete/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var applicant = _applicantService.Get((int)id, ref errorMessage);
            SetErrorMessage(errorMessage);
            if (applicant == null)
            {
                return NotFound();
            }

            return View(applicant);
        }

        // POST: Applicants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var errorMessage = string.Empty;
            _applicantService.Delete(id, ref errorMessage);
            SetErrorMessage(errorMessage);
            return RedirectToAction(nameof(Index));
        }

        private void SetPostDropdown(Applicant applicant)
        {
            ViewData["PostID"] = new SelectList(_applicantService.Set(), "PostID", "Title", applicant.PostID);
        }

        private void SetErrorMessage(string errorMessage)
        {
            ViewData["ErrorMessage"] = errorMessage;
        }
    }
}
