﻿using JOB.Common.ActionFilters;
using JOB.Common.Constants;
using JOB.Common.Utils;
using JOB.Entities.Entity;
using JOB.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JOB.App.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostService _postService;
        private readonly IApplicantService _applicantService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="postService"></param>
        /// <param name="applicantService"></param>
        public PostController(
            IPostService postService,
            IApplicantService applicantService)
        {
            _postService = postService;
            _applicantService = applicantService;
        }

        // GET: Post
        public async Task<IActionResult> Index(
            string sortOrder,
            string currentFilter,
            string searchString,
            int? pageNumber)
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString(Constants.TOKEN_SAMPLE)))
            {
                HttpContext.Session.SetString(Constants.TOKEN_SAMPLE, "Token Timestamp: " + DateTime.Now.ToString());
            }
            var tokenSample = HttpContext.Session.GetString(Constants.TOKEN_SAMPLE);
            ViewData[Constants.TOKEN_SAMPLE] = tokenSample;

            ViewData["CurrentSort"] = sortOrder;
            ViewData["NameSortParm"] = string.IsNullOrEmpty(sortOrder) ? Constants.NAME_DESC : Constants.NAME_ASC;
            ViewData["DateSortParm"] = sortOrder == Constants.DATE_ASC ? Constants.DATE_DESC : Constants.DATE_ASC;
            ViewData["SalarySortParm"] = sortOrder == Constants.SALARY_ASC ? Constants.SALARY_DESC : Constants.SALARY_ASC;

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;

            var errorMessage = string.Empty;
            var query = _postService.GetAll(ref errorMessage);
            var post = from s in query.Include(c => c.Employer) select s;

            if (!string.IsNullOrEmpty(searchString))
            {
                post = post.Where(s => s.Title.Contains(searchString));
            }

            post = sortOrder switch
            {
                Constants.NAME_ASC => post.OrderBy(s => s.Title),
                Constants.NAME_DESC => post.OrderByDescending(s => s.Title),
                Constants.SALARY_ASC => post.OrderBy(s => s.Salary),
                Constants.SALARY_DESC => post.OrderByDescending(s => s.Salary),
                Constants.DATE_ASC => post.OrderBy(s => s.Timestamp),
                _ => post.OrderByDescending(s => s.Timestamp),
            };

            SetErrorMessage(errorMessage);
            return View(await Pagination<Post>.CreateAsync(post.AsNoTracking(), pageNumber ?? 1, Constants.PAGE_SIZE));
        }

        // GET: Post/Details/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var post = _postService.Get((int)id, ref errorMessage);
            if (post == null)
            {
                return NotFound();
            }

            GetApplicantData(post, ref errorMessage);
            SetErrorMessage(errorMessage);
            return View(post);
        }

        // GET: Post/Create
        public IActionResult Create()
        {
            SetEmployerDropdown(new Post());
            return View();
        }

        // POST: Post/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("PostID,EmployerID,Timestamp,Title,Description,Salary")] Post post)
        {
            if (ModelState.IsValid)
            {
                var errorMessage = string.Empty;
                _postService.Create(post, ref errorMessage);
                SetErrorMessage(errorMessage);
                return RedirectToAction(nameof(Index));
            }
            SetEmployerDropdown(post);
            return View(post);
        }

        // GET: Post/Edit/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var post = _postService.Get((int)id, ref errorMessage);
            SetErrorMessage(errorMessage);
            if (post == null)
            {
                return NotFound();
            }
            SetEmployerDropdown(post);
            return View(post);
        }

        // POST: Post/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("PostID,EmployerID,Timestamp,Title,Description,Salary")] Post post)
        {
            if (id != post.PostID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var errorMessage = string.Empty;
                _postService.Update(id, post, ref errorMessage);
                SetErrorMessage(errorMessage);
                return RedirectToAction(nameof(Index));
            }
            SetEmployerDropdown(post);
            return View(post);
        }

        // GET: Post/Delete/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var post = _postService.Get((int)id, ref errorMessage);
            SetErrorMessage(errorMessage);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Post/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var errorMessage = string.Empty;
            _postService.Delete(id, ref errorMessage);
            SetErrorMessage(errorMessage);
            return RedirectToAction(nameof(Index));
        }

        private void GetApplicantData(Post post, ref string errorMessage)
        {
            var applicants = _applicantService.GetAll(ref errorMessage).Where(m => m.PostID == post.PostID);
            var viewModel = new List<Applicant>();
            foreach (var applicant in applicants)
            {
                viewModel.Add(applicant);
            }
            ViewData["Applicants"] = viewModel;
        }

        private void SetEmployerDropdown(Post post)
        {
            ViewData["EmployerID"] = new SelectList(_postService.Set(), "ID", "Profile", post.EmployerID);
        }

        private void SetErrorMessage(string errorMessage)
        {
            ViewData["ErrorMessage"] = errorMessage;
        }
    }
}
