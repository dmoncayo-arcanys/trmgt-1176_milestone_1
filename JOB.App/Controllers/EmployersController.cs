﻿using JOB.Common.ActionFilters;
using JOB.Entities.Entity;
using JOB.Services.Interface;
using Microsoft.AspNetCore.Mvc;

namespace JOB.App.Controllers
{
    public class EmployersController : Controller
    {
        private readonly IEmployerService _employerService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="employerService"></param>
        public EmployersController(IEmployerService employerService)
        {
            _employerService = employerService;
        }

        // GET: Employers
        public IActionResult Index()
        {
            var errorMessage = string.Empty;
            return View(_employerService.GetAll(ref errorMessage));
        }

        // GET: Employers/Details/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var employer = _employerService.Get((int)id, ref errorMessage);
            if (employer == null)
            {
                return NotFound();
            }

            SetErrorMessage(errorMessage);
            return View(employer);
        }

        // GET: Employers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind("CompanyName,ID,EmailAddress,MobileNumber,FirstName,LastName")] Employer employer)
        {
            if (ModelState.IsValid)
            {
                var errorMessage = string.Empty;
                _employerService.Create(employer, ref errorMessage);
                SetErrorMessage(errorMessage);
                return RedirectToAction(nameof(Index));
            }
            return View(employer);
        }

        // GET: Employers/Edit/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var employer = _employerService.Get((int)id, ref errorMessage);
            SetErrorMessage(errorMessage);
            if (employer == null)
            {
                return NotFound();
            }
            return View(employer);
        }

        // POST: Employers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(int id, [Bind("CompanyName,ID,EmailAddress,MobileNumber,FirstName,LastName")] Employer employer)
        {
            if (id != employer.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var errorMessage = string.Empty;
                _employerService.Update(id, employer, ref errorMessage);
                SetErrorMessage(errorMessage);
                return RedirectToAction(nameof(Index));
            }
            return View(employer);
        }

        // GET: Employers/Delete/5
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var errorMessage = string.Empty;
            var employer = _employerService.Get((int)id, ref errorMessage);
            SetErrorMessage(errorMessage);
            if (employer == null)
            {
                return NotFound();
            }

            return View(employer);
        }

        // POST: Employers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            var errorMessage = string.Empty;
            _employerService.Delete(id, ref errorMessage);
            SetErrorMessage(errorMessage);
            return RedirectToAction(nameof(Index));
        }

        private void SetErrorMessage(string errorMessage)
        {
            ViewData["ErrorMessage"] = errorMessage;
        }
    }
}
