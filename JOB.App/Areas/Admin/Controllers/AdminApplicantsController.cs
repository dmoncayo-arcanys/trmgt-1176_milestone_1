﻿using JOB.Entities.Entity;
using JOB.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace JOB.App.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AdminApplicantsController : Controller
    {
        private readonly DBContext _context;

        public AdminApplicantsController(DBContext context)
        {
            _context = context;
        }

        // GET: Admin/AdminApplicants
        public async Task<IActionResult> Index()
        {
            var dBContext = _context.Applicant.Include(a => a.Post);
            return View(await dBContext.ToListAsync());
        }

        // GET: Admin/AdminApplicants/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicant = await _context.Applicant
                .Include(a => a.Post)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (applicant == null)
            {
                return NotFound();
            }

            return View(applicant);
        }

        // GET: Admin/AdminApplicants/Create
        public IActionResult Create()
        {
            ViewData["PostID"] = new SelectList(_context.Post, "PostID", "PostID");
            return View();
        }

        // POST: Admin/AdminApplicants/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PostID,Message,YearsOfExperience,ExpectedSalary,StartDate,ID,EmailAddress,MobileNumber,FirstName,LastName")] Applicant applicant)
        {
            if (ModelState.IsValid)
            {
                _context.Add(applicant);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["PostID"] = new SelectList(_context.Post, "PostID", "PostID", applicant.PostID);
            return View(applicant);
        }

        // GET: Admin/AdminApplicants/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicant = await _context.Applicant.FindAsync(id);
            if (applicant == null)
            {
                return NotFound();
            }
            ViewData["PostID"] = new SelectList(_context.Post, "PostID", "PostID", applicant.PostID);
            return View(applicant);
        }

        // POST: Admin/AdminApplicants/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PostID,Message,YearsOfExperience,ExpectedSalary,StartDate,ID,EmailAddress,MobileNumber,FirstName,LastName")] Applicant applicant)
        {
            if (id != applicant.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(applicant);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicantExists(applicant.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["PostID"] = new SelectList(_context.Post, "PostID", "PostID", applicant.PostID);
            return View(applicant);
        }

        // GET: Admin/AdminApplicants/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var applicant = await _context.Applicant
                .Include(a => a.Post)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (applicant == null)
            {
                return NotFound();
            }

            return View(applicant);
        }

        // POST: Admin/AdminApplicants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var applicant = await _context.Applicant.FindAsync(id);
            _context.Applicant.Remove(applicant);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ApplicantExists(int id)
        {
            return _context.Applicant.Any(e => e.ID == id);
        }
    }
}
