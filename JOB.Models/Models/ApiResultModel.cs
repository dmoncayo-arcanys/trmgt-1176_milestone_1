﻿namespace JOB.Models.Models
{
    public class ApiResultModel : BaseApiResultModel
    {
        public string ErrorMessage { get; set; }

        public string SuccessMessage { get; set; }
    }
}
