﻿namespace JOB.Models.Models
{
    public enum Status
    {
        Error,
        Success
    }

    public class BaseApiResultModel
    {
        public object Response { get; set; }

        public Status Status { get; set; }
    }
}
