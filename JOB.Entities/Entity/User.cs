﻿using System.ComponentModel.DataAnnotations;

namespace JOB.Entities.Entity
{
    public class User
    {
        public int ID { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Email Address cannot be longer than 150 characters.")]
        [Display(Name = "Email Address")]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(13, ErrorMessage = "Mobile Number cannot be longer than 150 characters.")]
        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Last name cannot be longer than 50 characters.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Full Name")]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
    }
}
