﻿using System.ComponentModel.DataAnnotations;

namespace JOB.Entities.Entity
{
    public class Employer : User
    {
        [Required]
        [StringLength(50, ErrorMessage = "Company Name cannot be longer than 50 characters.")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Display(Name = "Profile")]
        public string Profile
        {
            get
            {
                return CompanyName + ": " + FirstName + " " + LastName;
            }
        }
    }
}
