﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JOB.Entities.Entity
{
    public class Applicant : User
    {
        public int PostID { get; set; }

        [Required]
        [StringLength(150, ErrorMessage = "Application Message cannot be longer than 150 characters.")]
        [Display(Name = "Application Message")]
        public string Message { get; set; }

        [Required]
        [Display(Name = "Years of Experience")]
        public int YearsOfExperience { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        [Display(Name = "Expected Salary")]
        public decimal ExpectedSalary { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Start Date")]
        public DateTime StartDate { get; set; }

        public Post Post { get; set; }
    }
}
