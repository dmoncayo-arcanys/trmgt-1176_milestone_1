﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace JOB.Entities.Entity
{
    public class Post
    {
        [Key]
        public int PostID { get; set; }

        public int EmployerID { get; set; }

        public DateTime Timestamp { get; set; }

        [StringLength(50, MinimumLength = 3)]
        public string Title { get; set; }

        public string Description { get; set; }

        [DataType(DataType.Currency)]
        [Column(TypeName = "money")]
        public decimal Salary { get; set; }

        public Employer Employer { get; set; }
    }
}
