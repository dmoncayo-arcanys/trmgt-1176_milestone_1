﻿using JOB.Entities.Entity;
using System.Linq;

namespace JOB.Services.Interface
{
    public interface IPostService
    {
        public IQueryable<Post> GetAll(ref string errorMessage);

        public Post Get(int id, ref string errorMessage);

        public void Update(int id, Post post, ref string errorMessage);

        public void Create(Post post, ref string errorMessage);

        public void Delete(int id, ref string errorMessage);

        public IQueryable<Employer> Set();
    }
}
