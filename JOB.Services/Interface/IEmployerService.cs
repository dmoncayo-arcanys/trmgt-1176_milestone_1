﻿using JOB.Entities.Entity;
using System.Linq;

namespace JOB.Services.Interface
{
    public interface IEmployerService
    {
        public IQueryable<Employer> GetAll(ref string errorMessage);

        public Employer Get(int id, ref string errorMessage);

        public void Update(int id, Employer post, ref string errorMessage);

        public void Create(Employer post, ref string errorMessage);

        public void Delete(int id, ref string errorMessage);
    }
}
