﻿using JOB.Entities.Entity;
using System.Linq;

namespace JOB.Services.Interface
{
    public interface IApplicantService
    {
        public IQueryable<Applicant> GetAll(ref string errorMessage);

        public Applicant Get(int id, ref string errorMessage);

        public void Update(int id, Applicant post, ref string errorMessage);

        public void Create(Applicant post, ref string errorMessage);

        public void Delete(int id, ref string errorMessage);

        public IQueryable<Post> Set();
    }
}
