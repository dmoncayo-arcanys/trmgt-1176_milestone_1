﻿using JOB.Common.Constants;
using JOB.Entities.Entity;
using JOB.Repositories.Interface;
using JOB.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace JOB.Services.Service
{
    public class PostService : IPostService
    {
        private readonly IRepository _repository;
        private readonly ILogger<PostService> _logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public PostService(
            IRepository repository,
            ILogger<PostService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        /// <summary>
        /// Get all post data including employer data.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns>IQueryable</returns>
        public IQueryable<Post> GetAll(ref string errorMessage)
        {
            try
            {
                return _repository.All<Post>()
                    .Include(c => c.Employer)
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// Get specific post data including employer data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Post</returns>
        public Post Get(int id, ref string errorMessage)
        {
            Post post = null;
            try
            {
                post = _repository.All<Post>()
                    .Include(c => c.Employer)
                    .AsNoTracking()
                    .FirstOrDefault(m => m.PostID == id);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
            return post;
        }

        /// <summary>
        /// Update a specific post data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="post"></param>
        /// <param name="errorMessage"></param>
        public void Update(int id, Post post, ref string errorMessage)
        {
            try
            {
                var entity = Get(id, ref errorMessage);
                if (entity != null)
                {
                    post.Timestamp = DateTime.Now;
                    _repository.Update<Post>(post);
                }
                else
                {
                    throw new NullReferenceException(Messages.ERROR_DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Create post data.
        /// </summary>
        /// <param name="post"></param>
        /// <param name="errorMessage"></param>
        public void Create(Post post, ref string errorMessage)
        {
            try
            {
                post.Timestamp = DateTime.Now;
                _repository.Create<Post>(post);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Deleted post data.
        /// </summary>
        /// <param name="post"></param>
        /// <param name="errorMessage"></param>
        public void Delete(int id, ref string errorMessage)
        {
            try
            {
                var entity = _repository.Find<Post>(x => x.PostID == id);
                _repository.Delete<Post>(entity);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Get Set object.
        /// </summary>
        /// <returns>IQueryable</returns>
        public IQueryable<Employer> Set()
        {
            return _repository.Set<Employer>();
        }
    }
}
