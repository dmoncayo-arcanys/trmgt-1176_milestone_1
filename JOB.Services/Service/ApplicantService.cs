﻿using JOB.Common.Constants;
using JOB.Entities.Entity;
using JOB.Repositories.Interface;
using JOB.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace JOB.Services.Service
{
    public class ApplicantService : IApplicantService
    {
        private readonly IRepository _repository;
        private readonly ILogger<ApplicantService> _logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public ApplicantService(
            IRepository repository,
            ILogger<ApplicantService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        /// <summary>
        /// Get all applicant data including post data.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns>IQueryable</returns>
        public IQueryable<Applicant> GetAll(ref string errorMessage)
        {
            try
            {
                return _repository.All<Applicant>()
                    .Include(c => c.Post)
                    .Include(c => c.Post.Employer)
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// Get specific applicant data including post data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Applicant</returns>
        public Applicant Get(int id, ref string errorMessage)
        {
            Applicant applicant = null;
            try
            {
                applicant = _repository.All<Applicant>()
                    .Include(c => c.Post)
                    .Include(c => c.Post.Employer)
                    .AsNoTracking()
                    .FirstOrDefault(m => m.ID == id);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
            return applicant;
        }

        /// <summary>
        /// Update a specific applicant data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="applicant"></param>
        /// <param name="errorMessage"></param>
        public void Update(int id, Applicant applicant, ref string errorMessage)
        {
            try
            {
                var entity = Get(id, ref errorMessage);
                if (entity != null)
                {
                    _repository.Update<Applicant>(applicant);
                }
                else
                {
                    throw new NullReferenceException(Messages.ERROR_DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Create applicant data.
        /// </summary>
        /// <param name="applicant"></param>
        /// <param name="errorMessage"></param>
        public void Create(Applicant applicant, ref string errorMessage)
        {
            try
            {
                _repository.Create<Applicant>(applicant);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Deleted applicant data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="errorMessage"></param>
        public void Delete(int id, ref string errorMessage)
        {
            try
            {
                var entity = _repository.Find<Applicant>(x => x.ID == id);
                _repository.Delete<Applicant>(entity);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Get Set object.
        /// </summary>
        /// <returns>IQueryable</returns>
        public IQueryable<Post> Set()
        {
            return _repository.Set<Post>();
        }
    }
}