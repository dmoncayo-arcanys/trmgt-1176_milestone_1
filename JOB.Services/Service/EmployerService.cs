﻿using JOB.Common.Constants;
using JOB.Entities.Entity;
using JOB.Repositories.Interface;
using JOB.Services.Interface;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;

namespace JOB.Services.Service
{
    public class EmployerService : IEmployerService
    {
        private readonly IRepository _repository;
        private readonly ILogger<EmployerService> _logger;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        public EmployerService(
            IRepository repository,
            ILogger<EmployerService> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        /// <summary>
        /// Get all employer data.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <returns>IQueryable</returns>
        public IQueryable<Employer> GetAll(ref string errorMessage)
        {
            try
            {
                return _repository.All<Employer>()
                    .AsNoTracking();
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
            return null;
        }

        /// <summary>
        /// Get specific employer data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="errorMessage"></param>
        /// <returns>Employer</returns>
        public Employer Get(int id, ref string errorMessage)
        {
            Employer employer = null;
            try
            {
                employer = _repository.All<Employer>()
                    .AsNoTracking()
                    .FirstOrDefault(m => m.ID == id);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
            return employer;
        }

        /// <summary>
        /// Update a specific employer data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employer"></param>
        /// <param name="errorMessage"></param>
        public void Update(int id, Employer employer, ref string errorMessage)
        {
            try
            {
                var entity = Get(id, ref errorMessage);
                if (entity != null)
                {
                    _repository.Update<Employer>(employer);
                }
                else
                {
                    throw new NullReferenceException(Messages.ERROR_DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Create employer data.
        /// </summary>
        /// <param name="employer"></param>
        /// <param name="errorMessage"></param>
        public void Create(Employer employer, ref string errorMessage)
        {
            try
            {
                _repository.Create<Employer>(employer);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }

        /// <summary>
        /// Deleted employer data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="errorMessage"></param>
        public void Delete(int id, ref string errorMessage)
        {
            try
            {
                var entity = _repository.Find<Employer>(x => x.ID == id);
                _repository.Delete<Employer>(entity);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                _logger.LogError(ex.ToString());
            }
        }
    }
}
