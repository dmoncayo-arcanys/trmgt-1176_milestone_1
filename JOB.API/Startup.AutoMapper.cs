﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace JOB.API
{
    public partial class Startup
    {
        private void ConfigureAutoMapper(IServiceCollection services)
        {
            var mapperConfiguration = new MapperConfiguration(config =>
            {
                config.AddProfile(new AutoMapperProfileConfiguration());
            });

            services.AddSingleton(sp => mapperConfiguration.CreateMapper());
        }

        private class AutoMapperProfileConfiguration : Profile
        {
            public AutoMapperProfileConfiguration()
            { 
                
            }
        }
    }
}
