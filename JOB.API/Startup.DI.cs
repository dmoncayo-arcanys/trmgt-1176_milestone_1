﻿using JOB.Repositories.Interface;
using JOB.Repositories.Repository;
using JOB.Services.Interface;
using JOB.Services.Service;
using Microsoft.Extensions.DependencyInjection;

namespace JOB.API
{
    public partial class Startup
    {
        private void ConfigureDI(IServiceCollection services)
        {
            services.AddScoped<IApplicantService, ApplicantService>();
            services.AddScoped<IEmployerService, EmployerService>();
            services.AddScoped<IPostService, PostService>();

            services.AddScoped<IRepository, Repository>();
        }
    }
}
