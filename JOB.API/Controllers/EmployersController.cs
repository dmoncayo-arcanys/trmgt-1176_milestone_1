﻿using JOB.Common.Constants;
using JOB.Entities.Entity;
using JOB.Models.Models;
using JOB.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace JOB.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployersController : ControllerBase
    {
        private readonly IEmployerService _employerService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="employerService"></param>
        public EmployersController(IEmployerService employerService)
        {
            _employerService = employerService;
        }

        /// <summary>
        /// GET: api/Employers
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetEmployer()
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            var post = _employerService.GetAll(ref errorMessage).ToList();

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.Response = post;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// GET: api/Employers/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        [HttpGet("{id}")]
        public IActionResult GetEmployer(int id)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            var post = _employerService.Get(id, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.Response = post;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// PUT: api/Employers/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employer"></param>
        /// <returns>Task</returns>
        [HttpPut("{id}")]
        public IActionResult PutEmployer(int id, Employer employer)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _employerService.Update(id, employer, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_UPDATED;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// POST: api/Employers
        /// </summary>
        /// <param name="employer"></param>
        /// <returns>Task</returns>
        [HttpPost]
        public IActionResult PostEmployer(Employer employer)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _employerService.Create(employer, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_SAVED;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// DELETE: api/Employers/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteEmployer(int id)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _employerService.Delete(id, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_DELETED;
            }

            return Ok(resultModel);
        }
    }
}
