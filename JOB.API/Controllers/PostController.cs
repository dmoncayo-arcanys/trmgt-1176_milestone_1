﻿using JOB.Common.Constants;
using JOB.Entities.Entity;
using JOB.Models.Models;
using JOB.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace JOB.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="postService"></param>
        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        /// <summary>
        /// GET: api/Post
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetPost()
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            var post = _postService.GetAll(ref errorMessage).ToList();

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.Response = post;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// GET: api/Post/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        [HttpGet("{id}")]
        public IActionResult GetPost(int id)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            var post = _postService.Get(id, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.Response = post;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// PUT: api/Post/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="post"></param>
        /// <returns>Task</returns>
        [HttpPut("{id}")]
        public IActionResult PutPost(int id, Post post)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _postService.Update(id, post, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_UPDATED;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// POST: api/Post
        /// </summary>
        /// <param name="post"></param>
        /// <returns>Task</returns>
        [HttpPost]
        public IActionResult PostPost(Post post)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _postService.Create(post, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_SAVED;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// DELETE: api/Post/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult DeletePost(int id)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _postService.Delete(id, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_DELETED;
            }

            return Ok(resultModel);
        }
    }
}
