﻿using JOB.Common.Constants;
using JOB.Entities.Entity;
using JOB.Models.Models;
using JOB.Services.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace JOB.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicantsController : ControllerBase
    {
        private readonly IApplicantService _applicantService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="applicantService"></param>
        public ApplicantsController(IApplicantService applicantService)
        {
            _applicantService = applicantService;
        }

        /// <summary>
        /// GET: api/Applicants
        /// </summary>
        /// <returns>IActionResult</returns>
        [HttpGet]
        public IActionResult GetApplicant()
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            var post = _applicantService.GetAll(ref errorMessage).ToList();

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.Response = post;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// GET: api/Applicants/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task</returns>
        [HttpGet("{id}")]
        public IActionResult GetApplicant(int id)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            var post = _applicantService.Get(id, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.Response = post;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// PUT: api/Applicants/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="applicant"></param>
        /// <returns>Task</returns>
        [HttpPut("{id}")]
        public IActionResult PutApplicant(int id, Applicant applicant)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _applicantService.Update(id, applicant, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_UPDATED;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// POST: api/Applicants
        /// </summary>
        /// <param name="applicant"></param>
        /// <returns>Task</returns>
        [HttpPost]
        public IActionResult PostApplicant(Applicant applicant)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _applicantService.Create(applicant, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_SAVED;
            }

            return Ok(resultModel);
        }

        /// <summary>
        /// DELETE: api/Applicants/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult DeleteApplicant(int id)
        {
            ApiResultModel resultModel = new();

            var errorMessage = string.Empty;
            _applicantService.Delete(id, ref errorMessage);

            if (errorMessage.Length > 0)
            {
                resultModel.Status = Status.Error;
                resultModel.ErrorMessage = errorMessage;
            }
            else
            {
                resultModel.Status = Status.Success;
                resultModel.SuccessMessage = Messages.SUCCESS_DATA_DELETED;
            }

            return Ok(resultModel);
        }
    }
}
