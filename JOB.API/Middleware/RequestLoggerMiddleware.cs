﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace JOB.API.Middleware
{
	public static class RequestLoggerExtensions
	{
		/// <summary>
		/// Use request logger.
		/// </summary>
		/// <param name="builder"></param>
		/// <returns></returns>
		public static IApplicationBuilder UseRequestLogger(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<RequestLoggerMiddleware>();
		}
	}

	/// <summary>
	/// Declare RequestLoggerMiddleware
	/// </summary>
	public class RequestLoggerMiddleware
	{
		private readonly RequestDelegate _next;
		private readonly ILogger _logger;
		private DateTime _start;
		private DateTime _end;

		/// <summary>
		/// Request logger middleware.
		/// </summary>
		/// <param name="next"></param>
		/// <param name="loggerFactory"></param>
		public RequestLoggerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
		{
			_next = next;
			_logger = loggerFactory.CreateLogger<RequestLoggerMiddleware>();
		}

		/// <summary>
		/// Invoke.
		/// </summary>
		/// <param name="context"></param>
		/// <returns></returns>
		public async Task Invoke(HttpContext context)
		{
			_start = DateTime.Now;
			await _next.Invoke(context);

			_end = DateTime.Now;

			TimeSpan ts = _end - _start;

			if (ts.TotalMilliseconds > 60000)
			{
				_logger.LogError("{@Milliseconds}s {@RequestPath}",
					ts.TotalSeconds.ToString("0.0"), context.Request.Path.Value);
			}
			else if (ts.TotalMilliseconds > 5000)
			{
				_logger.LogWarning("{@Milliseconds}s {@RequestPath}",
					ts.TotalSeconds.ToString("0.0"), context.Request.Path.Value);
			}
			else
			{
				_logger.LogInformation("{@Milliseconds}s {@RequestPath}",
					ts.TotalSeconds.ToString("0.0"), context.Request.Path.Value);
			}
		}
	}
}
