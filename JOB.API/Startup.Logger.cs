﻿using JOB.API.Middleware;
using JOB.Common.Constants;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Filters;
using System;
using System.IO;


namespace JOB.API
{
    public partial class Startup
    {
        private string CreateLogDirectory()
        {
            string logDir = Configuration.GetSection("Logging:LogDirectory").Value;
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
            }

            return Path.Combine(logDir, string.Format("{0}", DateTime.Today.ToString("yyyyMM")));
        }

        private long GetMaxLogSizeSetting()
        {
            string maxFileSizeByte = Configuration.GetSection("Logging:LogMaxByteSize").Value;

            if (long.TryParse(maxFileSizeByte, out long fileSizeLimit))
            {
                return fileSizeLimit;
            }

            return Constants.LOG_FILE_SIZE_LIMIT;
        }

        private void ConfigureLogger(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            string logDir = CreateLogDirectory();
            long fileSizeLimit = GetMaxLogSizeSetting();

            var defaultLogLevel = Configuration.GetSection("Logging")
                                .GetValue<LogLevel>("LogLevel:Default");
            var logEventLevel = (Serilog.Events.LogEventLevel)defaultLogLevel;

            var serilogger = new LoggerConfiguration()
                .MinimumLevel.Is(logEventLevel)
                .Enrich.FromLogContext()
                .WriteTo.File(logDir + "\\log_.txt",
                    rollingInterval: RollingInterval.Day, fileSizeLimitBytes: fileSizeLimit, rollOnFileSizeLimit: true)
                .WriteTo.Logger(lc => lc
                                .Filter.ByIncludingOnly(le => le.Level == Serilog.Events.LogEventLevel.Debug)
                                .WriteTo.File(logDir + "\\log_Debug_.txt",
                                    rollingInterval: RollingInterval.Day, fileSizeLimitBytes: fileSizeLimit,
                                    rollOnFileSizeLimit: true))
                .WriteTo.Logger(lc => lc
                                .Filter.ByIncludingOnly(le => le.Level == Serilog.Events.LogEventLevel.Warning)
                                .WriteTo.File(logDir + "\\log_Warning_.txt",
                                    rollingInterval: RollingInterval.Day, fileSizeLimitBytes: fileSizeLimit,
                                    rollOnFileSizeLimit: true))
                .WriteTo.Logger(lc => lc
                                .Filter.ByIncludingOnly(le => le.Level == Serilog.Events.LogEventLevel.Error)
                                .WriteTo.File(logDir + "\\log_Error_.txt",
                                    rollingInterval: RollingInterval.Day, fileSizeLimitBytes: fileSizeLimit,
                                    rollOnFileSizeLimit: true))
                .WriteTo.Logger(lc => lc
                                .MinimumLevel.Information()
                                .Filter.ByIncludingOnly(Matching.FromSource<RequestLoggerMiddleware>())
                                .WriteTo.File(logDir + "\\log_Request_.txt",
                                    rollingInterval: RollingInterval.Day, fileSizeLimitBytes: fileSizeLimit,
                                    rollOnFileSizeLimit: true))
                .WriteTo.Logger(lc => lc
                                .MinimumLevel.Information()
                                .Filter.ByIncludingOnly(Matching.FromSource<IRelationalCommandBuilderFactory>())
                                .WriteTo.File(logDir + "\\log_Sql_.txt",
                                    rollingInterval: RollingInterval.Day, fileSizeLimitBytes: fileSizeLimit,
                                    rollOnFileSizeLimit: true))
                .CreateLogger();

            loggerFactory.AddSerilog(serilogger);
            app.UseRequestLogger();
        }
    }
}
