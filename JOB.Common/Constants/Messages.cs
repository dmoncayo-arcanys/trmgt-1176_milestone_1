﻿namespace JOB.Common.Constants
{
    public class Messages
    {
        public const string ERROR_DATA_NOT_FOUND = "Data not found.";
        public const string ERROR_NULL_OBJECT = "Object is null.";
        public const string SUCCESS_DATA_SAVED = "Data has been saved.";
        public const string SUCCESS_DATA_UPDATED = "Data has been updated.";
        public const string SUCCESS_DATA_DELETED = "Data has been deleted.";
    }
}
