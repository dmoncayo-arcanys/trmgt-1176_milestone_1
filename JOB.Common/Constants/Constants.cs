﻿namespace JOB.Common.Constants
{
    public class Constants
    {
        // DB
        public const string DB_CONTEXT = "JobContext";

        // Pagination
        public const int PAGE_SIZE = 2;

        // Token
        public const string TOKEN_SAMPLE = "TOKEN_SAMPLE";

        // Sort
        public const string NAME_ASC = "name_asc";
        public const string NAME_DESC = "name_desc";
        public const string DATE_ASC = "date_asc";
        public const string DATE_DESC = "date_desc";
        public const string SALARY_ASC = "salary_asc";
        public const string SALARY_DESC = "salary_desc";

        // Tables
        public const string TABLE_APPLICATION = "Applicant";
        public const string TABLE_EMPLOYER = "Employer";
        public const string TABLE_POST = "Post";
        public const string TABLE_USER = "User";

        // Others
        public const long LOG_FILE_SIZE_LIMIT = 100000000;

    }
}
